const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];

(function kata1() {
    document.write('<h1>Kata 1</h1>')
    for (let num = 1; num <= 20; num++) {
        document.write(num, ", ")
    }
})();

(function kata2() {
    document.write('<h1>Kata 2</h1>')
    for (let num = 2; num <= 20; num++) {
        if (num % 2 === 0) {
            document.write(num, ", ")
        }
    }
})();
(function kata3() {
    document.write("<h1>Kata 3</h1>")
    for (let num = 1; num < 20; num++) {
        if (num % 2 === 1) {
            document.write(num, ", ")
        }
    }
})();

(function kata4() {
    document.write("<h1>Kata 4</h1>")
    for (let num = 5; num <= 100; num++) {
        if (num % 5 === 0) {
            document.write(num, ", ")
        }
    }
})();
(function kata5() {
    document.write("<h1>Kata 5</h1>")
    for (let num = 1; num <= 100; num++) {
        if (num % Math.sqrt(num) === 0) {
            document.write(num, ", ")
        }
    }
})();

(function kata6() {
    document.write("<h1>Kata 6</h1>")
    for (let num = 20; num > 0; num--) {
        document.write(num, ", ")
    }
})();    
(function kata7() {
    document.write("<h1>Kata 16</h1>")
    for (let num = 20; num > 1; num--) {
        if (num % 2 === 0) {
            document.write(num, ", ")
        }
    }
})();    
(function kata8() {
    document.write("<h1>Kata 8</h1>")
    for (let num = 20; num > 0; num--) {
        if (num % 2 === 1) {
            document.write(num, ", ")
        }
    }
})();
(function kata9() {
    document.write("<h1>Kata 9</h1>")
    for (let num = 100; num >= 5; num--) {
        if (num % 5 === 0) {
            document.write(num, ", ")
        }
    }
})();    
(function kata10() {
    document.write("<h1>Kata 10</h1>")
    for (let num = 100; num > 0; num--) {
        if (num % Math.sqrt(num) === 0) {
            document.write(num, ", ")
        }
    }
})();   
(function kata11() {
    document.write("<h1>Kata 11</h1>")
    for (let i = 0; i <= sampleArray.length; i++) {
        if (sampleArray[i] != undefined) {
            document.write(sampleArray[i], ", ")
        }
    }
})();    
(function kata12() {
    document.write("<h1>Kata 12</h1>")
    for (let i = 0; i <= sampleArray.length; i++) {
        if (sampleArray[i] % 2 === 0) {
            document.write(sampleArray[i], ", ")
        }
    }
})();    
(function kata13() {
    document.write("<h1>Kata 13</h1>")
    for (let i = 0; i <= sampleArray.length; i++) {
        if (sampleArray[i] % 2 === 1) {
            document.write(sampleArray[i], ", ")
        }
    }
})();  
(function kata14() {
    document.write("<h1>Kata 14</h1>")
    for (let i = 0; i <= sampleArray.length; i++) {
        if (sampleArray[i] != undefined) {
            document.write(sampleArray[i] * sampleArray[i], ", ")
        }
    }
})();    
(function kata15(){
    document.write()
    let sum = 0
    for (let i = 1; i <= 20; i++){
        sum += i;
    }
    document.write(sum)
})();

(function kata16() {
    document.write("<h1>Kata 16</h1>")
    let elements = 0 
    for (let i = 0; i < sampleArray.length; i++) {
            elements += sampleArray[i]
        }
    document.write(elements)
})();  
function kata17(sampleArray) {
    document.write("<h1>Kata 17</h1>")
    document.write(Math.min.apply(null, sampleArray));
}
kata17(sampleArray);

function kata18(sampleArray) {
    document.write("<h1>Kata 18</h1>")
    document.write(Math.max.apply(null, sampleArray));
}
kata18(sampleArray);

function kata19() {
    document.write("<h1>Kata 19</h1>")
    for (let i = 1; i < 21; i++) {
        const div = document.createElement("div")
        div.style.width = "100px"
        div.style.height = "20px"
        div.style.backgroundColor = "gray"
        div.style.margin = "5px"

    const destination = document.querySelector("body")
    destination.appendChild(div)

    }
}
kata19();

function kata20() {
    document.write("<h1>Kata 20</h1>")
    for (let i = 5; i <= 100; i++) {
        if (i % 5 === 0) {
            const div = document.createElement("div")
            div.style.width = 100 + i + "px"
            div.style.height = "20px"
            div.style.backgroundColor = "gray"
            div.style.margin = "5px"

            const destination = document.querySelector("body")
            destination.appendChild(div)
        }
    }
}
kata20();